import Vue from 'vue'
import axios from 'axios'
import router from '@/router'
import {
    VueAxios
} from './axios'
import {
    Notify
} from 'vant';

// 创建 axios 实例
const service = axios.create({
    baseURL: process.env.VUE_APP_API_BASE_URL, // api base_url
    timeout: 600000, // 请求超时时间
    onUploadProgress: function (progressEvent) {
        progressEvent;
        // Do whatever you want with the native progress event
        // alert('total: ' + progressEvent.total + ',loaded: ' + progressEvent.loaded)
    },
})

const err = (error) => {
    if (error.response) {
        const data = error.response.data
        // const token = Vue.ls.get('token')
        // const token = '7486bab3-3b3b-49fd-bca8-3c64b3aeb965'
        // todo: 需要对503状态吗进行处理
        if (error.response.status === 401 && !(data.result && data.result.isLogin)) {
            // notification.error({
            //     message: 'Unauthorized',
            //     description: 'Authorization verification failed'
            // })
            Notify('登陆过期，请重新登录。');
            Vue.ls.clear()
            router.push('/')
        }
        if (error.response.status === 400) {
            alert('')
        }
        if (error.response.status === 403) {
            // notification.error({
            //     message: 'Forbidden',
            //     description: data.message
            // })
            Notify('服务器错误');
        }
        if (error.response.status === 503) {
            Notify('服务器错误');
        }
        if (error.response.status === 500) {
            router.push('/')
            error;
            // this.$message({
            //     message: '警告哦，这是一条警告消息',
            //     type: 'warning'
            // });
        }

    }
    return Promise.reject(error)
}

// request interceptor
service.interceptors.request.use(config => {
    const token = Vue.ls.get('token')
    // const token = '7486bab3-3b3b-49fd-bca8-3c64b3aeb965'
    if (token) {
        config.headers['Authorization'] = 'Bearer ' + token
    }
    config.headers['Access-Control-Allow-Origin'] = '*'
    return config
}, err)

// response interceptor
service.interceptors.response.use((response) => {
    return response.data
}, err)

const installer = {
    vm: {},
    install(Vue) {
        Vue.use(VueAxios, service)
    }
}

export {
    installer as VueAxios,
    service as axios
}