// import SockJS from 'sockjs-client'
// import Stomp from 'stompjs'
import Vue from 'vue'
// const BASE_URL = 'ws://192.168.25.54:19006/starfish'
// const BASE_URL = 'ws://192.168.63.202:9999/grouper'
// const BASE_URL = 'ws://10.9.0.61:58105/grouper'
// const BASE_URL = 'ws://192.168.63.162:9999/grouper' 

const WS_BASE_URL = process.env.VUE_APP_WS_URL
// console.log(process.env.VUE_APP_IMG_URL)
// console.log()
// const BASE_URL = 'ws://192.168.123.232:7777/grouper'
import store from '@/store/index'
import { Notify } from 'vant';
let socket = null
let lockReconnect = false
export function WSconnect(id='',wsType='') {
    

    let timeout = null
    let timer = null
    // 建立连接对象
    // console.log(Vue.ls.get('districtID'))
    if(socket){
        if(lockReconnect){
            let params = {type: 'accept',districtId: id}
            socket.send(JSON.stringify(params))
        }else{  
            socket = new WebSocket(WS_BASE_URL)
            Vue.prototype.globalSocket = socket
        } 
    }else{
        socket = new WebSocket(WS_BASE_URL)
        Vue.prototype.globalSocket = socket
    }
    
    
    // 获取stomp子协议的客户端对象
    // let stompClient = Stomp.over(socket)
    // // 向服务器发起websocket连接并发送connect帧
    // stompClient.connect(
    //     {},
    //     function connectCallback(res){
    //         // stompClient.subscribe(topic_url,res=>{
    //         //     console.log(res)
    //         // })
    //         res;
    //         let json = {
    //             type:'register',
    //             districtId:'190220'
    //         }
    //         socket.send(json)
    //     },
    //     function errorCallback() {
    //         // 连接失败时再次进行调用函数
    //         clearTimeout(timeout)
    //         timeout = setTimeout(()=>{
    //           connect()
    //         },4000)
    //     }
    // )
    socket.onopen = () => {
        lockReconnect = true
        if(wsType=='jobs'){
            let json = {
                type: 'register',
                districtId: id
            }
            let jsons = {
                type: 'jobs',
                districtId: id
            }
            socket.send(JSON.stringify(json))
            socket.send(JSON.stringify(jsons))
            
        }else{
            let json = {
                type: 'register',
                districtId: id
            }
            let jsons = {
                type: 'accept',
                districtId: id
            }
            socket.send(JSON.stringify(json))
            socket.send(JSON.stringify(jsons))
        }
        
    }
    socket.onerror = () => {
        // reconnect()
    }
    socket.onmessage = (evt) => {
        let wsdata = JSON.parse(evt.data)
        // console.log(wsdata)
        if(wsdata?.type=='accept'){
            Notify({ type: 'primary', message: wsdata.data.url });
            store.commit('ADD_WS_DATA',wsdata.data)
            console.log(wsdata)
        }else if(wsdata?.type=='jobs'){
            store.commit('ADD_TASK_DATA',wsdata)
        }
        
        Vue;
        store;
        
    };
    socket.onclose = (state) => {
        if(state.reason == 'zhudong'){
            lockReconnect = false
            // Vue.prototype.globalSocket = null
        }else{
            lockReconnect = false
            reconnect(Vue.ls.get('districtID'))
        }
        
    };


    let json = {
        type: 'heartbeat',
        // districtId: Vue.ls.get('districtId')
        districtId: Vue.ls.get('districtID')
    }

    timer && clearInterval(timer)
    timer = setInterval(() => {
        if(Vue.prototype.globalSocket !== null){
            Vue.prototype.globalSocket.send(JSON.stringify(json))
        }else{
            Vue.prototype.globalSocket;
        }
    }, 5000);
    
    
    
    

    function reconnect(id) {
        if(lockReconnect){
            return
        }else{
            timeout && clearTimeout(timeout)
            timeout = setTimeout(() => {
                console.log(socket)
                WSconnect(id,'')
            }, 4000)
        }
        
    }
}