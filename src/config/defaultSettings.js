export default {
    storageOptions: {
        namespace: 'pro__',
        name: 'ls',
        storage: 'local'
    }
}