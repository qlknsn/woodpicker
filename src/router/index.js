import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'Login',
    meta: {
      title: "登录"
    },
    component: () => import('@/views/chuodian/login.vue')
  },
  {
    path: '/user',
    name: 'user',
    meta: {
      title: "个人信息"
    },
    component: () => import('@/views/chuodian/user.vue')
  },
  {
    path: '/tasklist',
    name: 'home',
    meta: {
      title: "任务列表"
    },
    component: () => import('@/views/chuodian/taskList.vue')
  },
  {
    path: '/map',
    name: 'Map',
    meta: {
      title: "地图选点"
    },
    component: () => import('@/views/chuodian/map.vue')
  },
  {
    path: '/pictures',
    name: 'Pictures',
    meta: {
      title: '照片列表'
    },
    component: () => import('@/views/chuodian/pictures.vue')
  },
  {
    path: '/picture/list',
    name: 'pictureList',
    meta: {
      title: '结果列表'
    },
    component: () => import('@/views/chuodian/pictureList.vue')
  }
]

const router = new VueRouter({
  routes
})
router.beforeEach((to, from, next) => {
  window.document.title = to.meta.title || ''
  next()
})
export default router