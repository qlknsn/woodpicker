// tools.js
export const INIT_MAP = "INIT_MAP";

// login.js
export const LOGIN_USER = "LOGIN_USER";

// logic.js
export const SEARCH_AREA = "SEARCH_AREA";
export const SHOW_ACTION_SHEET = "SHOW_ACTION_SHEET";
export const HIDE_ACTION_SHEET = "HIDE_ACTION_SHEET";
export const SET_AREA_ID = "SET_AREA_ID";
export const TAKE_PHOTO = "TAKE_PHOTO";
export const SET_ACTIVE_PIC = "SET_ACTIVE_PIC";
export const HIDE_IMG_PHOTO_LIST = "HIDE_IMG_PHOTO_LIST";
export const SUBMIT_LOCATION = "SUBMIT_LOCATION";
export const CLEAR_LNG_LAT = "CLEAR_LNG_LAT";
export const SET_MAP_CENTER = "SET_MAP_CENTER";
export const SET_CURRENT_LNGLAT = "SET_CURRENT_LNGLAT";
export const SHOW_IMG_PHOTO_LIST = "SHOW_IMG_PHOTO_LIST";
export const CLEAR_IMG_LIST = "CLEAR_IMG_LIST";
