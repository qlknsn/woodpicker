import api from '@/store/api/chuodian/urls.js'
import {
    axios
} from '@/utils/requests'


export function loginUser(data) {
    return axios({
        url: api.loginUser,
        data: data,
        method: 'post'
    })
}