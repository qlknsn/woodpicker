import api from "@/store/api/chuodian/urls.js";
import { axios } from "@/utils/requests";

export function searchArea(data) {
  return axios({
    url: api.searchArea,
    params: data,
  });
}

export function submitLocation(data) {
  return axios({
    url: api.submitLocation,
    data: data,
    method: "POST",
  });
}

export function takePhoto(params) {
  return axios({
    url: api.takePhoto,
    params: params,
  });
}
export function districtJobs(params) {
  return axios({
    url: api.districtJobs,
    params: params,
  });
}
export function deldistrictJobs(params) {
  return axios({
    url: `${api.districtJobs}/${params.id}`,
  });
}
export function singlePhotographV2(params) {
  return axios({
    url: api.singlePhotographV2,
    params: params,
  });
}
