import {
    loginUser
} from '@/store/api/chuodian/login.js'
import {
    LOGIN_USER
} from '@/store/mutation-types.js'
import router from '@/router'

const state = {}
const actions = {
    loginUser({
        commit
    }, data) {
        let p = loginUser(data)
        p.then(res => {
            console.log(res)
            commit('LOGIN_USER', res)
        })
    }
}
const mutations = {
    [LOGIN_USER](state, data) {
        state;
        data;
        if (data.code == 200) {
            router.push('/map')
        }
    }
}

export default {
    state,
    actions,
    mutations
}