import {
  searchArea,
  takePhoto,
  submitLocation,
  districtJobs,
  singlePhotographV2,
  deldistrictJobs
} from "@/store/api/chuodian/logic.js";
import { Notify } from "vant";
import store from "@/store/index";

import {
  SEARCH_AREA,
  SHOW_ACTION_SHEET,
  HIDE_ACTION_SHEET,
  SET_AREA_ID,
  TAKE_PHOTO,
  SET_ACTIVE_PIC,
  HIDE_IMG_PHOTO_LIST,
  SUBMIT_LOCATION,
  SHOW_IMG_PHOTO_LIST,
  CLEAR_IMG_LIST,
} from "@/store/mutation-types.js";
import router from '@/router'

const state = {
  takePhotoParams: { pageCount: 0, pageNo: 0 },
  areaList: [],
  actionSheetVisible: false,
  imgPopupVisible: false,
  areaID: "",
  imgList: [],
  currentActiveImgItem: "",
  districtJobList:[]
};
const actions = {
  submitLocation({ commit }, data) {
    let p = submitLocation(data);
    p.then((res) => {
      res;
      commit;
      if (res.code == 200) {
        Notify({
          type: "success",
          message: "提交成功",
        });
        commit("SUBMIT_LOCATION");
        commit("CLEAR_LNG_LAT");
        setTimeout(() => {
          router.push('/map')
          // Vue.prototype.globalSocket.close()
        }, 2000);
      } else {
        Notify("提交失败");
      }
    });
  },
  searchArea({ commit }, data) {
    let p = searchArea(data);
    p.then((res) => {
      commit("SEARCH_AREA", res);
    });
  },
  districtJobs({ commit }, data) {
    let p = districtJobs(data);
    p.then((res) => {
      commit("DISTRICT_JOBS", res);
    });
  },
  deldistrictJobs({commit},data){
    let p = deldistrictJobs(data);
    p.then((res) => {
      commit;
      res;
      store.dispatch('districtJobs')
    });
  },
  takePhoto({ commit }, data) {
    let p = takePhoto(data);
    p.then((res) => {
      commit("TAKE_PHOTO", res);
    });
  },
  singlePhotographV2({ commit }, data) {
    let p = singlePhotographV2(data);
    p.then((res) => {
      commit;
      res;
    });
  },
};
const mutations = {
  [SUBMIT_LOCATION](state) {
    state.currentActiveImgItem = "";
  },
  [SHOW_IMG_PHOTO_LIST](state) {
    state.imgPopupVisible = true;
  },
  [HIDE_IMG_PHOTO_LIST](state) {
    state.imgPopupVisible = false;
  },
  [SET_ACTIVE_PIC](state, data) {
    state.imgList.forEach((item) => {
      if (item.id == data.id) {
        Object.assign(item, {
          active: !item.active,
        });
        state.currentActiveImgItem = item;
      } else {
        Object.assign(item, {
          active: false,
        });
      }
    });
    state.imgList.push("");
    state.imgList.pop();
  },
  [CLEAR_IMG_LIST](state, data) {
    state.imgList = [];
    data;
  },
  [TAKE_PHOTO](state, data) {
    console.log(data)
    Notify(data.msg)
    state;
    data;
    // if (!state.imgPopupVisible) {
    //   state.imgList = [];
    // }
    // state.imgPopupVisible = true;
    // let d = data.data;
    // state.takePhotoParams = { pageCount: d.pageCount, pageNo: d.pageNo };
    // let { pageContent } = {
    //   ...d,
    // };
    // state.imgList = pageContent;
    // state.imgList.forEach((item) => {
    //   Object.assign(item, {
    //     active: false,
    //   });
    // });
    // const res = new Map();
    // let l = state.imgList.filter(
    //   (arr) => !res.has(arr.id) && res.set(arr.id, 1)
    // );
    // state.imgList = l;
  },
  [SET_AREA_ID](state, data) {
    state.areaID = data;
  },
  [SEARCH_AREA](state, data) {
    state;
    data;
    let d = data.data;
    state.areaList = d.pageContent;
    state.actionSheetVisible = true;
  },
  [SHOW_ACTION_SHEET](state) {
    state.actionSheetVisible = true;
  },
  [HIDE_ACTION_SHEET]() {
    state.actionSheetVisible = false;
  },
  DISTRICT_JOBS(state,res){
    state.districtJobList = res.data
  },
  ADD_TASK_DATA(state,res){
    state;
    // console.log(res)
    // let reptask = state.districtJobList.filter(r=>{
    //   return r.districtId == res.districtId
    // })
    let index = state.districtJobList.findIndex((item)=>{
      return item.districtId == res.districtId
    })
    if(index !== -1){
      
      state.districtJobList[index] = res.data[0]
      console.log(state.districtJobList)
    }
    // if(res.data[0])
  },
};

export default {
  state,
  actions,
  mutations,
};
