import {
  INIT_MAP,
  CLEAR_LNG_LAT,
  SET_MAP_CENTER,
  SET_CURRENT_LNGLAT,
} from "../mutation-types.js";
import AMapLoader from "@amap/amap-jsapi-loader";
import { Dialog } from "vant";

const state = {
  currentLngLat: "", //当前点的坐标
  locationStatus: false, // 是否成功定位的标志位
  globalMap: null,
  geolocation: null,
  currentEndLngLat: "", // 终点的坐标
  active:'/map',
  wsdataList:[],
  loadingstatus:true,
  currentDistrictId:''
};
const actions = {};
const mutations = {
  [SET_CURRENT_LNGLAT](state, data) {
    state.currentLngLat = `${data[0]}, ${data[1]}`;
  },
  [SET_MAP_CENTER](state, data) {
    console.log(data);
    state.globalMap.setCenter([data[0], data[1]]);
  },
  [CLEAR_LNG_LAT](state) {
    state.currentEndLngLat = "";
    state.currentLngLat = "";
  },
  [INIT_MAP](state, data) {
    data;
    AMapLoader.load({
      key: "14bf04e15750c35999275d63924f863c", // 申请好的Web端开发者Key，首次调用 load 时必填
      version: "2.0", // 指定要加载的 JSAPI 的版本，缺省时默认为 1.4.15
      plugins: [], //插件列表
    })
      .then((AMap) => {
        var map = new AMap.Map("real-map-container", {
          resizeEnable: true,
        });
        state.globalMap = map;
        AMap.plugin("AMap.Geolocation", function() {
          var geolocation = new AMap.Geolocation({
            // 是否使用高精度定位，默认：true
            enableHighAccuracy: true,
            // 设置定位超时时间，默认：无穷大
            // 定位按钮的停靠位置的偏移量，默认：Pixel(10, 20)
            buttonOffset: new AMap.Pixel(10, 20),
            //  定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
            zoomToAccuracy: true,
            //  定位按钮的排放位置,  RB表示右下
            buttonPosition: "RB",
            showMarker: false,
          });
          map.addControl(geolocation);
          geolocation.getCurrentPosition(function(status, result) {
            if (status != "complete") {
              Dialog({
                message: "定位失败，请开启GPS授权",
              });
            } else {
              state.locationStatus = true;
              state.currentLngLat = `${result.position.lng},${result.position.lat}`;

              Dialog({
                message: `定位成功，定位来源:(${result.location_type}),当前坐标(${result.position.lng},${result.position.lat})`,
              });
              var marker = new AMap.Marker({
                map: state.globalMap,
                size: new AMap.Size(30, 44),
                icon:
                  "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/mobile-point/start.png",
                position: [result.position.lng, result.position.lat],
                draggable: true,
              });
              //   var endMarker = new AMap.Marker({
              //     map: state.globalMap,
              //     size: new AMap.Size(30, 44),
              //     icon:
              //       "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/mobile-point/end.png",
              //     position: [result.position.lng + 0.00013, result.position.lat],
              //     draggable: true,
              //   });
              marker.on("dragging", function(e) {
                state.currentLngLat = `${e.lnglat.lng},${e.lnglat.lat}`;
              });
              //   endMarker.on("dragging", function(e) {
              //     state.currentEndLngLat = `${e.lnglat.lng},${e.lnglat.lat}`;
              //   });
            }
          });
        });
      })
      .catch((e) => {
        Dialog({
          message: e,
        });
      });
  },
  CHANGE_ACTIVE(state,res){
    state.active = res
  },
  ADD_WS_DATA(state,res){
    // console.log(res)
    // 查看重拍相机在数组的第几个位置
    let index = state.wsdataList.findIndex((item)=>{
      return item.id == res.id
    })
    if(index !== -1){
      // 更换图片url和状态
       state.wsdataList[index].loading = false    
       state.wsdataList[index].url = res.url    
    }else{
      // 新推过来的直接加到数组最后
      // console.log(res)
      res.loading = false
      if(state.currentDistrictId == res.districtId){
        state.wsdataList.push(res)
      }
    }
  },

  CLEAR_WS_DATA(state){
    state.wsdataList = []
  },
  SET_CURRENT_ID(state,index){
    state.currentDistrictId = index
  },
  CHECK_LOADING(state,index){
    // 重拍之后的加载状态
    console.log(state.wsdataList[index])
    state.wsdataList[index].loading = true
  },
  CHECK_ACTIVE(state,res){
    // 选中相机
    let index = state.wsdataList.findIndex((item)=>{
      return item.status == 0
    })
    if(index !== -1){
      state.wsdataList[index].status = 1
    }
    setTimeout(() => {
      state.wsdataList[res].status = 0
    }, 100);
    
  }
};
export default {
  state,
  actions,
  mutations,
};
