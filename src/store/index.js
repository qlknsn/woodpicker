import Vue from 'vue'
import Vuex from 'vuex'
import tools from './modules/tools'
import login from './modules/chuodian/login'
import logic from './modules/chuodian/logic'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    tools,
    login,
    logic,
  }
})