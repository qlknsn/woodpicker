module.exports = {
  outputDir:
    process.env.NODE_ENV === "production"
      ? `mobile-point-test`
      : "mobile-point-dev",
  configureWebpack: {
    externals: {},
    module: {
      rules: [
        {
          test: /\.(swf|ttf|eot|svg|woff(2))(\?[a-z0-9]+)?$/,
          loader: "file-loader",
        },
      ],
    },
  },
  // configureWebpack: (config) => {
  //     config.module.rules.push({
  //         test: /\.(swf|ttf|eot|svg|woff(2))(\?[a-z0-9]+)?$/,
  //         loader: 'file-loader',
  //     })
  //     config.module.rules.push({
  //         externals: {
  //             AMap: "AMap"
  //         }
  //     })
  // },
  devServer: {
    proxy: {
      "/url": {
        target: "http://192.168.25.100:8031/",
        // target: "http://192.168.63.162:8031/",
        // target: "http://192.168.123.232:8031/",
        // target: "https://58.247.128.138:449/",
        // target: "http://10.9.0.61:7031/",
        // target: "http://192.168.63.202:8031/",
        // target: "http://10.9.0.61:8041/",
        changeOrigin: true,
        pathRewrite: {
          "^/url": "",
        },
      },
    },
    // before: app => {}
  },
};
